import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Layout from './components/Layout/layout'
import BurgerBuilder from './containers/BurgerBuilder/burgerBuilder'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Layout>
          <p>Test</p>
          <BurgerBuilder/>
        
          </Layout>        
      </div>
    );
  }
}

export default App;
